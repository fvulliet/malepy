#ifndef __TEST_HELPERS_STDDUMP_HPP__
#define __TEST_HELPERS_STDDUMP_HPP__

#include <cppunit/TestAssert.h>
#include <string>

#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>

#include <QVariantMap>
#include <QVariantList>

#include <QByteArray>
#include <QString>
#include <QDateTime>

CPPUNIT_NS_BEGIN

#define CPPUNIT_TRAITS_OVERRIDE(TYPE) \
    template <> std::string assertion_traits<TYPE>::toString(const TYPE &t)

CPPUNIT_TRAITS_OVERRIDE(QByteArray);
CPPUNIT_TRAITS_OVERRIDE(QString);
CPPUNIT_TRAITS_OVERRIDE(QDateTime);

CPPUNIT_TRAITS_OVERRIDE(QJsonDocument);
CPPUNIT_TRAITS_OVERRIDE(QJsonObject);
CPPUNIT_TRAITS_OVERRIDE(QJsonArray);

CPPUNIT_TRAITS_OVERRIDE(QVariantMap);
CPPUNIT_TRAITS_OVERRIDE(QVariantList);

CPPUNIT_NS_END

#endif // TEST_HELPERS_STDDUMP_HPP
