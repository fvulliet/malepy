#include <QMetaObject>
#include <QCoreApplication>
#include <cppunit/TestRunner.h>
#include <QDebug>

#include "qt_runner.hpp"

using namespace CppUnit;

QtRunner::QtRunner(TestRunner& runner) :
    m_runner(runner),
    m_controller(NULL),
    m_path()
{
}

void QtRunner::run(TestResult& controller, const std::string& testPath)
{
    m_controller = &controller;
    m_path = testPath;

    QMetaObject::invokeMethod(this, "run", Qt::QueuedConnection);

    QCoreApplication::exec();
}

void QtRunner::run()
{
    m_runner.run(*m_controller, m_path);

    QCoreApplication::exit(0);
}
