#include <QDebug>
#include "test_helpers_stddump.hpp"

CPPUNIT_NS_BEGIN

CPPUNIT_TRAITS_OVERRIDE(QByteArray)
{
    QString out;
    QDebug dbg(&out);

    dbg << "QByteArray(" << t.toHex().toUpper().constData() << ")";
    return qPrintable(out);
}

CPPUNIT_TRAITS_OVERRIDE(QString)
{
    QString out;
    QDebug dbg(&out);

    dbg << "QString(" << t.toUtf8().constData() << ")";
    return qPrintable(out);
}

CPPUNIT_TRAITS_OVERRIDE(QDateTime)
{
    QString out;
    QDebug dbg(&out);

    dbg << t;
    return qPrintable(out);
}

CPPUNIT_TRAITS_OVERRIDE(QJsonDocument)
{
    QString out;
    QDebug dbg(&out);

    dbg << "QJsonDocument(" << t.toJson(QJsonDocument::Compact).constData() << ")";
    return qPrintable(out);
}

CPPUNIT_TRAITS_OVERRIDE(QJsonObject)
{
    QString out;
    QDebug dbg(&out);

    dbg << "QJsonObject(" << QJsonDocument(t).toJson(QJsonDocument::Compact).constData() << ")";
    return qPrintable(out);
}

CPPUNIT_TRAITS_OVERRIDE(QJsonArray)
{
    QString out;
    QDebug dbg(&out);

    dbg << "QJsonArray(" << QJsonDocument(t).toJson(QJsonDocument::Compact).constData() << ")";
    return qPrintable(out);
}

CPPUNIT_TRAITS_OVERRIDE(QVariantMap)
{
    QString out;
    QDebug dbg(&out);

    dbg << "QVariantMap(" << QJsonDocument::fromVariant(t).toJson(QJsonDocument::Compact).constData() << ")";
    return qPrintable(out);
}

CPPUNIT_TRAITS_OVERRIDE(QVariantList)
{
    QString out;
    QDebug dbg(&out);

    dbg << "QVariantList(" << QJsonDocument::fromVariant(t).toJson(QJsonDocument::Compact).constData() << ")";
    return qPrintable(out);
}

CPPUNIT_NS_END
