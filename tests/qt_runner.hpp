#ifndef __QT_RUNNER_HH__
#define __QT_RUNNER_HH__

#include <string>
#include <QObject>

namespace CppUnit {

class TestRunner;
class TestResult;

class QtRunner : public QObject
{
    Q_OBJECT

public:
    explicit QtRunner(TestRunner& runner);

    void run(TestResult& controller, const std::string& testPath = "");

protected Q_SLOTS:
    void run();

protected:
    TestRunner& m_runner;
    TestResult* m_controller;
    std::string m_path;
};

}

#endif
