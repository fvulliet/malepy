#ifndef __TEST_HELPERS_HH__
#define __TEST_HELPERS_HH__

#include <QObject>
#include <QMutex>
#include <QWaitCondition>

#include "config.h"

namespace testHelpers
{

class SignalWaiter : public QObject
{
    Q_OBJECT

public:
    SignalWaiter(QObject *obj, const char *sig);

    bool wait(unsigned long timeout);

    void reset();

    void setExpectedCount(int count);

    inline int count() const
    { return m_count; }

protected Q_SLOTS:
    void wake();

protected:
    QObject *m_obj;
    int m_count;
    int m_expectedCount;
    QMutex m_lock;
    QWaitCondition m_cond;
};

}
#endif
