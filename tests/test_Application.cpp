#include "test_helpers.hpp"
#include "test_helpers_stddump.hpp"

#include <iostream>

#include "test_Application.hpp"

#include "classes/Screen.hpp"
#include "classes/Display.hpp"
#include "classes/Segment.hpp"
#include "classes/Measure.hpp"
#include "classes/Digit.hpp"
#include "classes/Icon.hpp"

void ApplicationTest::setUp()
{
}

void ApplicationTest::tearDown()
{
}

void ApplicationTest::main()
{
    // Screen is the link between "conceptual" elements and "real" segments
    // it knows the command and bit related to each real segment
    Screen myScreen;

    // ask screen to create "conceptual" widgets
    Digit* digit12_1 = myScreen.createDigit(MEAS12, COL1);
    Digit* digit12_2 = myScreen.createDigit(MEAS12, COL2);
    Digit* digit12_3 = myScreen.createDigit(MEAS12, COL3);
    Digit* digit12_4 = myScreen.createDigit(MEAS12, COL4);

    Digit* digit13_1 = myScreen.createDigit(MEAS13, COL1);
    Digit* digit13_2 = myScreen.createDigit(MEAS13, COL2);
    Digit* digit13_3 = myScreen.createDigit(MEAS13, COL3);
    Digit* digit13_4 = myScreen.createDigit(MEAS13, COL4);

    Digit* digit23_1 = myScreen.createDigit(MEAS23, COL1);
    Digit* digit23_2 = myScreen.createDigit(MEAS23, COL2);
    Digit* digit23_3 = myScreen.createDigit(MEAS23, COL3);
    Digit* digit23_4 = myScreen.createDigit(MEAS23, COL4);

    Segment* unit12 = myScreen.createUnit(MEAS12);
    Segment* unit13 = myScreen.createUnit(MEAS13);
    Segment* unit23 = myScreen.createUnit(MEAS23);

    Icon* wifiIcon = myScreen.createWifiIcon();
    Icon* wapIcon = myScreen.createWapIcon();

    // once all the elements (=widgets) have been created, you can create
    // more advanced widgets
    Measure* m12 = new Measure(digit12_1, digit12_2, digit12_3, digit12_4, unit12);
    Measure* m13 = new Measure(digit13_1, digit13_2, digit13_3, digit13_4, unit13);
    Measure* m23 = new Measure(digit23_1, digit23_2, digit23_3, digit23_4, unit23);

    // register elements so that they're notified by the timer
    myScreen.registerElement(wifiIcon);
    myScreen.registerElement(wapIcon);
    myScreen.registerElement(digit12_1);

    // start the screen timer
    myScreen.startCounting();

    // Display is the top most widget, which gathers all widgets
    Display disp;
    disp.setMeasure12(m12);
    disp.setMeasure13(m13);
    disp.setMeasure23(m23);
    disp.setWapIcon(wapIcon);
    disp.setWifiIcon(wifiIcon);

    // once Display has been filled with widgets, you can play around !
    disp.showWap(false);
    disp.showWifi(true);
    disp.writeMeas12(42, true);
    disp.editMeas12(0);

    // end of application, delete all allocations
    delete digit12_1;
    delete digit12_2;
    delete digit12_3;
    delete digit12_4;

    delete digit13_1;
    delete digit13_2;
    delete digit13_3;
    delete digit13_4;

    delete digit23_1;
    delete digit23_2;
    delete digit23_3;
    delete digit23_4;

    delete unit12;
    delete unit13;
    delete unit23;

    delete wifiIcon;
    delete wapIcon;
}
