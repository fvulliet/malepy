#include <QCoreApplication>
#include <QTimer>
#include <QAbstractEventDispatcher>

#include "test_helpers.hpp"

namespace testHelpers
{

SignalWaiter::SignalWaiter(
        QObject *obj,
        const char *sig) :
    QObject(obj),
    m_obj(obj),
    m_count(0),
    m_expectedCount(1)
{
    connect(obj, sig,
            this, SLOT(wake()));
}

bool SignalWaiter::wait(unsigned long timeout)
{
    QMutexLocker l(&m_lock);

    if (m_count >= m_expectedCount)
        return true;
    else if (m_obj->thread() == thread())
    {
        QTimer t;
        t.setSingleShot(true);
        t.start(timeout);

        while ((m_count < m_expectedCount) && t.isActive())
        {
            l.unlock();
            QCoreApplication::processEvents(QEventLoop::AllEvents |
                    QEventLoop::WaitForMoreEvents, 100);
            l.relock();
        }
    }
    else
        m_cond.wait(&m_lock, timeout);
    return m_count >= m_expectedCount;
}

void SignalWaiter::wake()
{
    QMutexLocker l(&m_lock);
    ++m_count;
    m_cond.wakeAll();
}

void SignalWaiter::reset()
{
    m_count = 0;
}

void SignalWaiter::setExpectedCount(int count)
{
    m_expectedCount = count;
}

}
