#! /bin/bash

#####
# Thus script gather every tasks that needs to be done for bootstrapping the
# projet. It should be run once after cloning the projet
###

## Install git hooks
pushd .git/hooks/
  ln -s ../../scripts/pre-commit-checkstyle.sh pre-commit
popd
