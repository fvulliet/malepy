#!/bin/bash
#installation
#   cd my_gitproject
#   ln -s ../../pre-commit-checkstyle.sh .git/hooks/pre-commit
#   chmod +x pre-commit.sh


OPTIONS="--style=allman  --indent=spaces=4 --indent-switches --align-pointer=type --align-reference=type --convert-tabs --attach-namespaces --pad-header --pad-oper --add-brackets"
#OPTIONS="--style=whitesmith  --indent=spaces=4 --indent-switches --align-pointer=type --align-reference=type --convert-tabs --attach-namespaces --pad-header --pad-oper --add-brackets"
RETURN=0
ASTYLE=$(which astyle)
if [ $? -ne 0 ]; then
    echo "[!] astyle not installed. Unable to check source file format policy." >&2
    exit 0
fi

# Support old version of astyle 2.04
less -f $(which astyle) | grep 2.04
BUG=$?

FILES=$(git diff --cached --name-only --diff-filter=ACMR | grep -E "\.(c|cpp|h|hpp)$")
for FILE in $FILES; do
    if [ $BUG -ne 0 ]; then
        $ASTYLE $OPTIONS < $FILE | cmp -s $FILE -
    else
        $ASTYLE $OPTIONS < $FILE | head -c -1 | cmp -s $FILE -
    fi
    if [ $? -ne 0 ]; then
#        echo "$?" >&2
#        echo "[!] $FILE does not respect the agreed coding style." >&2
#		echo "you can run: $ASTYLE $OPTIONS -n $FILE" >&2
	#run directement astyle
	$ASTYLE $OPTIONS -n $FILE
        RETURN=1
    fi
done


exit $RETURN
