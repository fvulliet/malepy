#! /usr/bin/env python3
# -*- encoding: utf-8 -*-
import re

HEADER = """/**
 * @file {header_filename}
 * @brief FIXME Description
 * @copyright Chauvin-Arnoux
 */
#ifndef {include_guard}
#define {include_guard}

namespace {namespace} {{

/************************** CONST & MACRO **************************/

/****************************** ENUM *******************************/

/***************************** STRUCT ******************************/

/***************************** CLASS *******************************/

/**
 * @brief
 */
class {classname}
{{
public:

private:

}};

}} //namespace {namespace}

#endif /* {include_guard} */

"""

SOURCE = """/**
 * @file {source_filename}
 * @brief FIXME Description
 * @copyright Chauvin-Arnoux
 */
#include "{header_filename}"

namespace {namespace} {{

/************************ CONSTRUCTOR & DESTRUCTOR ***************************/

/************************ PUBLIC METHODS *************************************/

/************************ PRIVATE METHODS ************************************/

}}
"""

HEADER_IFACE = """/**
 * @file {header_filename}
 * @brief FIXME Description
 * @copyright Chauvin-Arnoux
 */
#ifndef {include_guard}
#define {include_guard}

namespace {namespace} {{

/************************** CONST & MACRO **************************/

/****************************** ENUM *******************************/

/***************************** STRUCT ******************************/

/***************************** CLASS *******************************/

/**
 * @brief
 */
class {classname}
{{
public:
    ///dtor
    virtual ~{classname}() {{}};


}};

}} //namespace {namespace}

#endif /* {include_guard} */

"""


def main():
	gentype = input("enter generator type (0/empty: class, 1 interface): ")
	namespace = input("enter namespace: ")
	classname = input("enter classname: ")

	source_filename = classname.lower() + ".cpp"
	header_filename = classname.lower() + ".hpp"
	opts = {
		"namespace" : namespace,
		"classname" : classname,
		"source_filename" : source_filename,
		"header_filename" : header_filename,
		"include_guard" : re.sub('(.)([A-Z][a-z]+)', r'\1_\2', classname).upper() + "_HPP_"

	}

	if gentype == "" or gentype == "0":
		with open(source_filename, "w+") as fd:
			fd.write(SOURCE.format(**opts))
		with open(header_filename, "w+") as fd:
			fd.write(HEADER.format(**opts))
	elif gentype == "1":
		with open(header_filename, "w+") as fd:
			fd.write(HEADER_IFACE.format(**opts))
	else:
		print("invalid generator type")

if __name__ == "__main__":
	main()
