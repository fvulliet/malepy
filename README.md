-DTESTS=ON -DCOVERAGE=ON -DCMAKE_PREFIX_PATH=/home/ca/Qt5.4.2/5.4/gcc_64/

# Dependencies

We assume Qt has been installed as a standalone kit, the installation
directory is defined as *${QT_ROOT_DIR}* int the rest of this
document. Be sure to indicate the full path rather than an alias

for instance

`export QT_ROOT_DIR=/home/ca/Qt5.4.2/5.4/gcc_64/`

but not

`export QT_ROOT_DIR=~/Qt5.4.2/5.4/gcc_64/`

# Compilation

## QtCreator

Open a new project and  select the CMakeLists.txt at the root path of
the projet. Be sure to select the toolkit where we installed our
dependencies.

You might have to add `-DCMAKE_PREFIX_PATH=${QT_ROOT_DIR}` to the
command line options of cmake

## command line

```bash
mkdir build
cd build
cmake ${PATH_TO_THE_PROJECT_ROOT} -DCMAKE_PREFIX_PATH=${QT_ROOT_DIR}
make
```

## unitary tests and code coverage
UT and CC require the following applications to be installed:
- cppunit: unitary tests framework; "q2app" must be compiled as a shared target ("libq2app"), which is linked with a dedicated "test_all" executable; this executable is linked with a static library ("testHelper") which gathers a new main ("test_main") and helpers.
cppunit is somehow managed through cmake (use cmake with the following arguments: -DTESTS=ON -DCOVERAGE=ON)

http://cppunit.sourceforge.net/doc/1.8.0/

- gcov: this is the actual GCC testing tool for code coverage; set the GCC compile option "--coverage" to create some "*.gcno" files next to "*.o" files; after executing the application, some "*.gcda" files are also generated; these files will be processed by lcov to generate pretty html reports
gcov is somehow managed through cmake

https://gcc.gnu.org/onlinedocs/gcc/Gcov-Intro.html#Gcov-Intro

- lcov: this is a graphical front-end for gcov
```bash
# (optionally) you can reset everything
lcov --zerocounters --directory ./
```
then once the test_all application has been built AND EXECUTED, move to the build directory and call
```bash
lcov --capture --directory ./ --output-file coverage.info
```
This will create a coverage.info file. Then call this next line to generate all the required html files in the "out" folder
```bash
genhtml --legend --branch-coverage coverage.info --output-directory out
```
Finally, open the index.html file in your browser, and let the magic happen..

http://ltp.sourceforge.net/coverage/lcov.php
