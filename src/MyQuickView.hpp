#ifndef __MYQUICKVIEW_HPP__
#define __MYQUICKVIEW_HPP__

#include <QQuickView>

class MyQuickView : public QQuickView
{
public:
    MyQuickView();
    explicit MyQuickView(QUrl url);

public:
    bool event(QEvent *event) override;
};

#endif /* __MYQUICKVIEW_HPP__ */




