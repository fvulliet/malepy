message(STATUS "CMake version: ${CMAKE_VERSION}")

include_directories(
    ${CMAKE_CURRENT_SOURCE_DIR}
)

add_subdirectory(classes)
add_subdirectory(qml/Malepy) # this way Malepy is the name of a module
qt5_add_resources(MALEPY_QRC qml/Malepy/Malepy.qrc)

# show *.qrc file in IDE
file(GLOB ALL_QRC_FILES "*.qrc")
add_custom_target(allqrcfiles SOURCES ${ALL_QRC_FILES})

##############
#  libMalepy  #
##############

set(LIBMALEPY_CORE_MODS Core Quick Gui Widgets)
add_library(libMalepy SHARED
    main.cpp
)
set_target_properties(libMalepy PROPERTIES
    OUTPUT_NAME malepy
    LINKER_LANGUAGE CXX
)
set(MALEPY_IO_LIBRARIES
    ${QT_LIBRARIES}
)
target_link_libraries(libMalepy
    mwClasses
    ${MALEPY_IO_LIBRARIES}
)
qt5_use_modules(libMalepy ${LIBMALEPY_CORE_MODS})

##############
#  Malepy  #
##############

add_executable(MalepyApp
    main.cpp
    ${MALEPY_QRC}
    MyGuiApplication.hpp MyGuiApplication.cpp
    MyQuickView.hpp MyQuickView.cpp
    MalepyQml.hpp MalepyQml.cpp
    CAFontLoader.hpp CAFontLoader.cpp
    ScreenSimulation.hpp ScreenSimulation.cpp
)
target_link_libraries(MalepyApp
    libMalepy
)

set(MALEPY_CORE_MODS ${LIBMALEPY_CORE_MODS})
qt5_use_modules(MalepyApp ${MALEPY_CORE_MODS})
