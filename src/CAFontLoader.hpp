#ifndef CAFONTLOADER_HPP
#define CAFONTLOADER_HPP

#include <QObject>
#include <QFont>

class CAFontLoader : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString source READ source WRITE setSource)
    Q_PROPERTY(QFont font READ font)

public:
    explicit CAFontLoader(QObject *parent = nullptr);

    QString name() const;
    void setName(QString name);

    QString source() const;
    void setSource(QString source);

    QFont font() const;

private:
    QString m_name;
    QString m_source;
    int m_id;
    QFont m_font;
};

#endif // CAFONTLOADER_HPP
