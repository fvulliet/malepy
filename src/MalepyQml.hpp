#ifndef __MALEPYQML_HPP__
#define __MALEPYQML_HPP__

class QQmlEngine;

/**
 * @brief MalepyQml is an helper class used to load QML module not
 * worrying about where it's installed.
 */
class MalepyQml
{
public:
    static bool load(QQmlEngine *engine);
};

#endif // __MALEPYQML_HPP__

