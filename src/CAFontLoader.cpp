#include <QFontDatabase>
#include <QDebug>
#include "CAFontLoader.hpp"

CAFontLoader::CAFontLoader(QObject *parent) :
    QObject(parent)
{
    qDebug() << "-----there";
}

QString CAFontLoader::name() const
{
    return m_name;
}

void CAFontLoader::setName(QString name)
{
    m_name = name;
}

QString CAFontLoader::source() const
{
    return m_source;
}

void CAFontLoader::setSource(QString source)
{
    m_id = QFontDatabase::addApplicationFont(source);

    if (m_id < 0)
    {
        qWarning() << "could not load" << source;
        return;
    }

    QStringList loadedFontFamilies = QFontDatabase::applicationFontFamilies(m_id);
    if (loadedFontFamilies.count() > 1)
        qWarning() << "loadedFontFamilies.count() > 1 !!";

    if (!loadedFontFamilies.empty())
    {
        m_font = QFont(loadedFontFamilies.at(0));
        m_source = source;
    }
}

QFont CAFontLoader::font() const
{
    return m_font;
}
