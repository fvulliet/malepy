#ifndef __SCREEN_H__
#define __SCREEN_H__

#include <QObject>

#define WIFI_ICON 100

#define MEASURE1_DIGIT1_SEG1 1
#define MEASURE1_DIGIT1_SEG2 2
#define MEASURE1_DIGIT1_SEG3 3
#define MEASURE1_DIGIT1_SEG4 4
#define MEASURE1_DIGIT1_SEG5 5
#define MEASURE1_DIGIT1_SEG6 6
#define MEASURE1_DIGIT1_SEG7 7

#define MEASURE1_DIGIT2_SEG1 8
#define MEASURE1_DIGIT2_SEG2 9
#define MEASURE1_DIGIT2_SEG3 10
#define MEASURE1_DIGIT2_SEG4 11
#define MEASURE1_DIGIT2_SEG5 12
#define MEASURE1_DIGIT2_SEG6 13
#define MEASURE1_DIGIT2_SEG7 14

enum measRow
{
    MEAS12 = 0,
    MEAS13,
    MEAS23
};

enum digitCol
{
    COL1 = 0,
    COL2,
    COL3,
    COL4
};

class ScreenSimulation : public QObject
{
    Q_OBJECT
    Q_PROPERTY(int network READ network WRITE setNetwork NOTIFY networkChanged)
    Q_PROPERTY(int batteryLevel READ batteryLevel WRITE setBatteryLevel NOTIFY batteryLevelChanged)
    Q_PROPERTY(bool recording READ recording WRITE setRecording NOTIFY recordingChanged)

public:
    explicit ScreenSimulation(QObject* parent = nullptr);
    ScreenSimulation(const ScreenSimulation &other);
    ~ScreenSimulation() {}

    int network() const;
    void setNetwork(int net);

    int batteryLevel() const;
    void setBatteryLevel(int level);

    bool recording() const;
    void setRecording(bool record);

signals:
    void networkChanged();
    void batteryLevelChanged();
    void recordingChanged();

private:
    int m_network;
    int m_batteryLevel;
    bool m_recording;
};

Q_DECLARE_METATYPE(ScreenSimulation)

#endif /* __SCREEN_H__ */
