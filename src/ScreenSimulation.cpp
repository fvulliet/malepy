#include "ScreenSimulation.hpp"

ScreenSimulation::ScreenSimulation(QObject* parent) :
    QObject(parent),
    m_network(0),
    m_batteryLevel(100),
    m_recording(false)
{
}

ScreenSimulation::ScreenSimulation(const ScreenSimulation& other) :
    QObject()
{
    m_network = other.m_network;
    m_batteryLevel = other.m_batteryLevel;
    m_recording = other.m_recording;
}

int ScreenSimulation::network() const
{
    return m_network;
}

void ScreenSimulation::setNetwork(int net)
{
    if (m_network != net)
    {
        m_network = net;
        emit networkChanged();
    }
}

int ScreenSimulation::batteryLevel() const
{
    return m_batteryLevel;
}

void ScreenSimulation::setBatteryLevel(int level)
{
    if (m_network != level)
    {
        m_network = level;
        emit batteryLevelChanged();
    }
}

bool ScreenSimulation::recording() const
{
    return m_recording;
}

void ScreenSimulation::setRecording(bool record)
{
    if (m_network != record)
    {
        m_network = record;
        emit recordingChanged();
    }
}
