import QtQuick 2.4
import Malepy 1.0

Item {
    id: awesome

    property alias icons: variables

    readonly property FontLoader fontAwesomeRegular: FontLoader {
        source: "./fa-regular-400.ttf"
    }
    readonly property FontLoader fontAwesomeSolid: FontLoader {
        source: "./fa-solid-900.ttf"
    }

    readonly property string regular: fontAwesomeRegular.name
    readonly property string solid: fontAwesomeSolid.name

    Awesome.Variables {
        id: variables
    }
}
