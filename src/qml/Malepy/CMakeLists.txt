# define extra files to be added in ProjectTree (IDE)
set(MALEPY_QML_EXTRA
    qmldir.in
)
# define Singleton
set(MALEPY_QML_SINGLETON
    Style.qml
    Fonts.qml
)
# define Public
set(MALEPY_QML_PUBLIC
    Display.qml
    Header.qml
    Footer.qml
    Segment.qml
    Measure.qml
    Digit.qml
    Icon.qml
    AwesomeVariables.qml
    FontAwesome.qml
    Connectivity.qml
    Alert.qml
    Units.qml
    Message.qml
    MessageUnits.qml
    FramedIcon.qml
    main.qml
)
# Shortcut for all QML files
set(MALEPY_QML
     ${MALEPY_QML_SINGLETON}
     ${MALEPY_QML_PUBLIC}
)

# compute required variables for qmldir configuration
foreach(QMLFILE ${MALEPY_QML_SINGLETON})
    get_filename_component(QMLCLASS "${QMLFILE}" NAME_WE)
    set(MALEPY_SINGLETON_QML "${MALEPY_SINGLETON_QML}\nsingleton ${QMLCLASS} 1.0 ${QMLFILE}")
endforeach(QMLFILE)

foreach(QMLFILE ${MALEPY_QML_PUBLIC})
    get_filename_component(QMLCLASS "${QMLFILE}" NAME_WE)
    set(MALEPY_PUBLIC_QML "${MALEPY_PUBLIC_QML}\n${QMLCLASS} 1.0 ${QMLFILE}")
endforeach(QMLFILE)

# configure qmldir
configure_file("qmldir.in" "${CMAKE_CURRENT_SOURCE_DIR}/qmldir" @ONLY)
# set plugin sources
set(PLUGIN_SRC MalepyQmlPlugin.hpp MalepyQmlPlugin.cpp)

add_library(malepyplugin MODULE ${PLUGIN_SRC})
target_link_libraries(malepyplugin libMalepy)
qt5_use_modules(malepyplugin Quick)
if(WIN32)
    set_target_properties(malepyplugin PROPERTIES PREFIX "")
endif(WIN32)

install(TARGETS malepyplugin
        LIBRARY DESTINATION "${CMAKE_INSTALL_LIBDIR}/qml/Malepy" COMPONENT Runtime
        RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}/qml/Malepy" COMPONENT Runtime)

# Copy files for qmlscene
add_files(QmlPluginFiles
    FILES ${MALEPY_QML} ${MALEPY_QML_EXTRA} qmldir
    DESTINATION "${CMAKE_CURRENT_BINARY_DIR}")

qmlscene(qmlscene ${CMAKE_CURRENT_BINARY_DIR}/main.qml ${MALEPY_QML} ${MALEPY_QML_EXTRA}
    IMPORT_PATH "${CMAKE_BINARY_DIR}/src/qml"
    LIB_PATH "${CMAKE_BINARY_DIR}/src")






## define Singleton
#set(QML_SINGLETON
#    Style.qml
# )
## define Public
#set(QML_PUBLIC
#    Malepy.qml
#    main.qml
#  )

#file(GLOB DOC_QML "*.qml")
#set(QML_FILES ${DOC_QML})
#add_custom_target(myqmlfiles ALL
#    SOURCES ${QML_FILES}
#)


