import QtQuick 2.4
import Malepy 1.0

Item {
    property int value: 0
    property string uName
    property string vName

    onValueChanged: setValue(value)

    function setValue(val) {
        digit1.value = String(Math.floor(val/1000))
        digit2.value = String(Math.floor((val - 1000*digit1.value)/100))
        digit3.value = String(Math.floor((val - 1000*digit1.value - 100*digit2.value)/10))
        digit4.value = String(Math.floor((val - 1000*digit1.value - 100*digit2.value - 10*digit3.value)))
    }

    Row {
        anchors.fill: parent

        Loader {
            height: parent.height
            width: parent.width*10/67
            sourceComponent: measureTitle
            onStatusChanged: {
                if (status === Loader.Ready) {
                    item.uName = uName
                    item.vName = vName
                }
            }
        }
        Loader {
            height: parent.height
            width: parent.width*10/67
            sourceComponent: hyphen
        }
        Item {
            height: parent.height
            width: parent.width*7/67

            Digit {
                id: digit1
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/67
            sourceComponent: point
        }
        Item {
            height: parent.height
            width: parent.width*7/67

            Digit {
                id: digit2
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/67
            sourceComponent: point
        }
        Item {
            height: parent.height
            width: parent.width*7/67

            Digit {
                id: digit3
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/67
            sourceComponent: point
        }
        Item {
            height: parent.height
            width: parent.width*7/67

            Digit {
                id: digit4
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
            }
        }
        Item {
            height: parent.height
            width: parent.width*3/67
        }
        Units {
            height: parent.height
            width: parent.width*7/67
        }
    }

    Component {
        id: point

        Item {
            Rectangle {
                width: 10; height: 2*width
                radius: 5
                color: "black"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
            }
        }
    }

    Component {
        id: hyphen

        Item {
            Rectangle {
                width: 3*height; height: 10
                radius: 5
                color: "black"
                anchors.centerIn: parent
            }
        }
    }

    Component {
        id: measureTitle

        Row {
            property string uName
            property string vName

            spacing: 5

        Item {
                height: parent.height; width: (parent.width-parent.spacing)/2

                Text {
                    text: uName
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignRight
                    font.bold: true
                    font.pixelSize: 20
                }
            }
        Item {
                height: parent.height; width: (parent.width-parent.spacing)/2

                Text {
                    text: vName
                    anchors.centerIn: parent
                    horizontalAlignment: Text.AlignLeft
                    font.bold: true
                    font.pixelSize: 20
                }
            }
        }
    }
}
