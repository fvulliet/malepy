pragma Singleton
import QtQuick 2.4

QtObject {
    readonly property string appFont: "Arial"
    readonly property real fontFactor: appFont === "Arial" ? 0.9 : 1

    // assuming 1280(H)*800(W)
    readonly property int defaultWidth: 800
    readonly property int defaultHeight: 600

    property int currentWidth: defaultWidth
    property int currentHeight: defaultHeight

    // layout
    readonly property real mainPageRatio: 0.95
    readonly property int mainPageAngle: 10
    readonly property real xMarginRatio: 1/30
    readonly property real yMarginRatio: 1/20
    readonly property real minimizerRatio: 1/30
    readonly property real mainMenuRatio: 1/8

    // heights
    readonly property int radioButtonHeight: 20 * currentHeight/defaultHeight
    readonly property int checkBoxHeight: 30 * currentHeight/defaultHeight
    readonly property int buttonHeight: 40 * currentHeight/defaultHeight
    readonly property int textInputHeight: 45 * currentHeight/defaultHeight
    readonly property int switchHeight: 22 * currentHeight/defaultHeight
    readonly property int dropdownHeight: 40 * currentHeight/defaultHeight
    readonly property int payloadTitleHeight: 40 * currentHeight/defaultHeight
    readonly property int scopeListElementHeight: 30 * currentHeight/defaultHeight
    readonly property int tabsHeight: 30 * currentHeight/defaultHeight
    readonly property int spinBoxSize: 30 * currentHeight/defaultHeight
    readonly property int listItemHeight: 40 * currentHeight/defaultHeight
    readonly property int sectionHeaderHeight: 30 * currentHeight/defaultHeight
    readonly property int treeElementHeight: 20 * currentHeight/defaultHeight
    readonly property int listHeaderSpacing: 10 * currentHeight/defaultHeight
    readonly property int listSpacing: 3 * currentHeight/defaultHeight
    readonly property int tabsHighlightHeight: 2 * currentHeight/defaultHeight
    readonly property int separatorHeight: 1 * currentHeight/defaultHeight

    // widths
    readonly property int buttonWidth: 150 * currentWidth/defaultWidth
    readonly property int switchWidth: 44 * currentWidth/defaultWidth
    readonly property int scrollBarWidth: 4 * currentWidth/defaultWidth
    readonly property int plusMinusControlsWidth: 30 * currentWidth/defaultWidth
    readonly property int extensibleSplitWidth: 40 * currentWidth/defaultWidth

    // others
    readonly property int minimalBorder: 1
    readonly property int thickBorder: 2
    readonly property int borderRadius: 3
    readonly property int cursorStroke: 4
    readonly property int highlightStroke: 4 * currentWidth/defaultWidth
    readonly property int h0FontSize: fontFactor * (appFont !== "Arial" ? currentHeight * 0.0375 : currentHeight * 0.035) // 30/28
    readonly property int h1FontSize: fontFactor * (appFont !== "Arial" ? currentHeight * 0.0325 : currentHeight * 0.03) // 26/24
    readonly property int h2FontSize: fontFactor * (appFont !== "Arial" ? currentHeight * 0.0275 : currentHeight * 0.025) // 22/20
    readonly property int h3FontSize: fontFactor * (appFont !== "Arial" ? currentHeight * 0.025 : currentHeight * 0.0225) // 20/18
    readonly property int h4FontSize: fontFactor * (appFont !== "Arial" ? currentHeight * 0.02 : currentHeight * 0.0175) // 16/14
    readonly property int h5FontSize: fontFactor * (appFont !== "Arial" ? currentHeight * 0.0175 : currentHeight * 0.015) // 14/12
    readonly property int h6FontSize: fontFactor * (appFont !== "Arial" ? currentHeight * 0.015 : currentHeight * 0.0125) // 12/10
    readonly property int titleFontSize: h2FontSize
    readonly property int subTitleFontSize: h3FontSize
    readonly property int commentFontSize: h4FontSize
    readonly property real topMenuIconRatio: 2/3
    readonly property real bottomMenuIconRatio: topMenuIconRatio
    readonly property int bottomMenuFontSize: h6FontSize
    readonly property int treeElementFontSize: h5FontSize
    readonly property real maxWidgetSize: 250 * currentHeight/defaultHeight
    readonly property real maxSmallWidgetSize: 150 * currentHeight/defaultHeight
    readonly property int shadowSize: 1
    readonly property int stdMargin: 10
    readonly property int borderMargin: 2
    readonly property int layoutGridGap: 10
    readonly property real darkerHover: 1.025
    readonly property real goldenNumber: 1.61803398875
    readonly property int headerGap: 1
}

