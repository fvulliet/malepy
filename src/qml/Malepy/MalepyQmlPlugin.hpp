#ifndef __MALEPYQMLPLUGIN_HPP__
#define __MALEPYQMLPLUGIN_HPP__

#include <QQmlExtensionPlugin>

/**
 * @brief Malepy QML plugin class
 */
class MalepyQmlPlugin : public QQmlExtensionPlugin
{
    Q_OBJECT
    Q_PLUGIN_METADATA(IID "MalepyQmlPlugin")

public:
    void registerTypes(const char *uri);
};

#endif // __MALEPYQMLPLUGIN_HPP__

