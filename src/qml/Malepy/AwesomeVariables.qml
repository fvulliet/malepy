import QtQuick 2.4

QtObject {
    readonly property string fa_wifi                               : "\uf1eb"
    readonly property string fa_ethernet                               : "\uf6ff"
    readonly property string fa_wap                               : "\uf3cd"
    readonly property string fa_500px                               : "\uf26e"
    readonly property string fa_accessible_icon                     : "\uf368"
    readonly property string fa_accusoft                            : "\uf369"
    readonly property string fa_address_book                        : "\uf2b9"
    readonly property string fa_address_card                        : "\uf2bb"
}
