import QtQuick 2.4
import Malepy 1.0

Rectangle {
    property alias lib: myIcon.font.family
    property alias code : myIcon.text
    property bool focused: false
    property int shadow: 10

    width: 1.3*height
    radius: 5
    color: "white"
    border.width: focused ? shadow : 1
    border.color: "black"

    Rectangle {
        id: innerRect
        height: parent.height-shadow; width: parent.width-shadow
        anchors.top: parent.top; anchors.left: parent.left
        anchors.leftMargin: 3; anchors.topMargin: 3
        radius: 5
        color: "white"

        Text {
            id: myIcon
            antialiasing: true
            scale: 1
            color: "black"
            font.pixelSize: parent.height*0.8
            opacity: 0
        }
    }
}
