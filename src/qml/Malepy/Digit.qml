import QtQuick 2.4
import Malepy 1.0

Item {
    property int thick: 12
    property int digitRadius: 6
    property color digitColor: "black"
    property string value: "0"

    onValueChanged: setValue(value)

    function setValue(val) {
        top.visible = false
        topRight.visible = false
        bottomRight.visible = false
        bottom.visible = false
        bottomLeft.visible = false
        topLeft.visible = false
        center.visible = false
        southWest.visible = false
        southEast.visible = false
        south.visible = false
        northWest.visible = false
        northEast.visible = false
        north.visible = false
        centerLeft.visible = false
        centerRight.visible = false

        switch (val) {
        case "0":
            top.visible = true
            topRight.visible = true
            bottomRight.visible = true
            bottom.visible = true
            bottomLeft.visible = true
            topLeft.visible = true
            break;
        case "1":
            topRight.visible = true
            bottomRight.visible = true
            break;
        case "2":
            top.visible = true
            topRight.visible = true
            bottom.visible = true
            bottomLeft.visible = true
            center.visible = true
            break;
        case "3":
            top.visible = true
            topRight.visible = true
            bottomRight.visible = true
            bottom.visible = true
            center.visible = true
            break;
        case "4":
            topRight.visible = true
            bottomRight.visible = true
            topLeft.visible = true
            center.visible = true
            break;
        case "5":
            top.visible = true
            bottomRight.visible = true
            bottom.visible = true
            topLeft.visible = true
            center.visible = true
            break;
        case "6":
            top.visible = true
            bottomRight.visible = true
            bottom.visible = true
            bottomLeft.visible = true
            topLeft.visible = true
            center.visible = true
            break;
        case "7":
            top.visible = true
            topRight.visible = true
            bottomRight.visible = true
            break;
        case "8":
            top.visible = true
            topRight.visible = true
            bottomRight.visible = true
            bottom.visible = true
            bottomLeft.visible = true
            topLeft.visible = true
            center.visible = true
            break
        case "9":
            top.visible = true
            topRight.visible = true
            bottomRight.visible = true
            bottom.visible = true
            topLeft.visible = true
            center.visible = true
            break;
        case "A":
            top.visible = true
            topRight.visible = true
            bottomRight.visible = true
            bottomLeft.visible = true
            topLeft.visible = true
            center.visible = true
//            southWest.visible = true
//            southEast.visible = true
//            northEast.visible = true
//            bottomRight.visible = true
//            topRight.visible = true
//            centerRight.visible = true
        break;
        }
    }

    Rectangle {
        id: top
        radius: digitRadius
        color: digitColor
        height: thick; width: parent.width
        anchors.top: parent.top
    }

    Rectangle {
        id: topRight
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        anchors.right: parent.right
        anchors.top: parent.top
    }

    Rectangle {
        id: bottomRight
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        anchors.right: parent.right
        anchors.bottom: parent.bottom
    }
    Rectangle {
        id: bottom
        radius: digitRadius
        color: digitColor
        height: thick; width: parent.width
        anchors.bottom: parent.bottom
    }

    Rectangle {
        id: bottomLeft
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        anchors.left: parent.left
        anchors.bottom: parent.bottom
    }

    Rectangle {
        id: topLeft
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        anchors.left: parent.left
        anchors.top: parent.top
    }

    Rectangle {
        id: center
        radius: digitRadius
        color: digitColor
        height: thick; width: parent.width
        anchors.centerIn: parent
    }

    Rectangle {
        id: centerLeft
        radius: digitRadius
        color: digitColor
        height: thick; width: parent.width/2
        anchors.centerIn: parent
    }

    Rectangle {
        id: centerRight
        radius: digitRadius
        color: digitColor
        height: thick; width: parent.width/2
        x: parent.width/2
        anchors.centerIn: parent
    }

    Rectangle {
        id: northWest
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        x: parent.width/4 - thick/2
        y: 0
        rotation: -45
    }

    Rectangle {
        id: north
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        x: parent.width/2 - thick/2
        y: 0
    }

    Rectangle {
        id: northEast
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        x: parent.width*3/4 - thick/2
        y: 0
        rotation: 45
    }

    Rectangle {
        id: southWest
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        x: parent.width/4 - thick/2
        y: parent.height/2
        rotation: 45
    }

    Rectangle {
        id: south
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        x: parent.width/2 - thick/2
        y: parent.height/2
    }

    Rectangle {
        id: southEast
        radius: digitRadius
        color: digitColor
        height: parent.height/2; width: thick
        x: parent.width*3/4 - thick/2
        y: parent.height/2
        rotation: -45
    }
}

