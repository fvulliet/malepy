import QtQuick 2.4
import Malepy 1.0

Row {
    function setNetwork(net) {
        myConnectivity.currentItem = net
    }

    Connectivity {
        id: myConnectivity
        height: parent.height; width: parent.width*3/8
    }
    Row {
        height: parent.height; width: parent.width*5/8

        Item {
            height: parent.height; width: parent.width/5

            Icon {
                lib: Fonts.fontAwesome
                code: "\uf6ad"
                color: "black"
                height: parent.height*3/4
                anchors.centerIn: parent
            }
        }
        Item {
            height: parent.height; width: parent.width/5

            Icon {
                lib: Fonts.fontAwesome
                code: "\uf6ad"
                color: "black"
                height: parent.height*3/4
                anchors.centerIn: parent
            }
        }
        Item {
            height: parent.height; width: parent.width/5

            Icon {
                lib: Fonts.fontAwesome
                code: "\uf6ad"
                color: "black"
                height: parent.height*3/4
                anchors.centerIn: parent
            }
        }
        Item {
            height: parent.height; width: parent.width/5

            Icon {
                lib: Fonts.fontAwesome
                code: "\uf6ad"
                color: "black"
                height: parent.height*3/4
                anchors.centerIn: parent
            }
        }
        Item {
            height: parent.height; width: parent.width/5

            Icon {
                lib: Fonts.fontAwesome
                code: "\uf6ad"
                color: "black"
                height: parent.height*3/4
                anchors.centerIn: parent
            }
        }
    }
}
