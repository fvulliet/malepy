#include <qqml.h>

#include "MalepyQmlPlugin.hpp"
#include "ScreenSimulation.hpp"

void MalepyQmlPlugin::registerTypes(const char *uri)
{
    Q_ASSERT(uri == QLatin1String("Malepy"));
    qmlRegisterType<ScreenSimulation>(uri, 1, 0, "ScreenSimulation");
}


