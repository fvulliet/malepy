import QtQuick 2.4
import Malepy 1.0

Row {
    property int currentItem: -1

    FontAwesome {
        id: awesome
    }

    Item {
        height: parent.height; width: parent.width/3

        Icon {
            lib: awesome.solid
            code: awesome.icons.fa_wap
            color: "black"
            height: parent.height*3/4
            anchors.centerIn: parent
            blinking: currentItem === 0
        }
    }
    Item {
        height: parent.height; width: parent.width/3

        Icon {
            lib: awesome.solid
            code: awesome.icons.fa_wifi
            color: "black"
            height: parent.height*3/4
            anchors.centerIn: parent
            blinking: currentItem === 1
        }
    }
    Item {
        height: parent.height; width: parent.width/3

        Icon {
            lib: awesome.solid
            code: awesome.icons.fa_ethernet
            color: "black"
            height: parent.height*3/4
            anchors.centerIn: parent
            blinking: currentItem === 2
        }
    }
}
