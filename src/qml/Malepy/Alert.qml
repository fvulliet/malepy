import QtQuick 2.4
import Malepy 1.0

Rectangle {
    property alias pixelSize: myText.font.pixelSize
    property alias text: myText.text
    property bool focused: false

    color: focused ? "black" : "white"

    Text {
        id: myText
        color: focused ? "white" : "black"
        font.pixelSize: 11 // default value
        font.bold: true
    }
}
