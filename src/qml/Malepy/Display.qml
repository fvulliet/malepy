import QtQuick 2.4
import Malepy 1.0

Rectangle {
    property int separatorHeight: 10

    anchors.fill: parent

    ScreenSimulation {
        id: myScreenSimulation

        onNetworkChanged: {
            console.log("----network changed", network)
            header.setNetwork(network)
        }
    }

    Column {
        anchors.fill: parent

        Header {
            id: header
            width: parent.width-10; height: (parent.height-2*separatorHeight)/7
            anchors.horizontalCenter: parent.horizontalCenter
        }
        Loader {
            width: parent.width; height: separatorHeight
            sourceComponent: separator
        }
        Column {
            id: payload
            width: parent.width-10; height: (parent.height-2*separatorHeight)*5/7
            anchors.horizontalCenter: parent.horizontalCenter
            spacing: 10

            Row {
                id: alertsAndMeasures
                width: parent.width; height: (parent.height-parent.spacing)*3/4

                Item {
                    height: parent.height; width: parent.width*10/77

                    Column {
                        id: alerts
                        anchors.fill: parent

                        Item {
                            width: parent.width; height: parent.height/4

                            Alert {
                                anchors.centerIn: parent
                                width: parent.width*0.9
                                height: parent.width/2
                                pixelSize: height*0.8
                                text: "OL"
                            }
                        }
                        Item {
                            width: parent.width; height: parent.height/4

                            Alert {
                                anchors.centerIn: parent
                                width: parent.width*0.9
                                height: parent.width/2
                                pixelSize: height*0.8
                                text: "MAX"
                            }
                        }
                        Item {
                            width: parent.width; height: parent.height/4

                            Alert {
                                anchors.centerIn: parent
                                width: parent.width*0.9
                                height: parent.width/2
                                pixelSize: height*0.8
                                text: "MIN"
                            }
                        }
                        Item {
                            width: parent.width; height: parent.height/4

                            Alert {
                                anchors.centerIn: parent
                                width: parent.width*0.9
                                height: parent.width/2
                                pixelSize: height*0.8
                                text: "THD"
                            }
                        }
                    }

                    Rectangle {
                        height: parent.height; width: 3
                        anchors.right: parent.right
                        color: "black"
                    }
                }

                Column {
                    id: measures
                    height: parent.height; width: parent.width*67/77
                    spacing: 10

                    Measure {
                        width: parent.width; height: (parent.height-2*parent.spacing)/3
                        value: 1234
                        uName: "U12"
                        vName: "V1"
                    }
                    Measure {
                        width: parent.width; height: (parent.height-2*parent.spacing)/3
                        value: 5678
                        uName: "U23"
                        vName: "V2"
                    }
                    Measure {
                        width: parent.width; height: (parent.height-2*parent.spacing)/3
                        value: 1994
                        uName: "U31"
                        vName: "V3"
                    }
                }
            }
            Message {
                width: parent.width; height: (parent.height-parent.spacing)*1/4
                value: "A1A2A3A"
            }
        }
        Loader {
            width: parent.width; height: separatorHeight
            sourceComponent: separator
        }
        Footer {
            id: footer
            width: parent.width-10; height: (parent.height-2*separatorHeight)/7
            anchors.horizontalCenter: parent.horizontalCenter
        }
    }

    Component {
        id: separator

        Item {
            Rectangle {
                width: parent.width-10; height: 3
                anchors.centerIn: parent
                color: "black"
            }
        }
    }
}


