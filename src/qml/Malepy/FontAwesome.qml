import QtQuick 2.4

Item {
    id: awesome

    property alias icons: variables

    readonly property FontLoader fontAwesomeRegular: FontLoader {
        source: "./fonts/fa-regular-400.otf"
    }
    readonly property FontLoader fontAwesomeSolid: FontLoader {
        source: "./fonts/fa-solid-900.otf"
    }

    readonly property string regular: fontAwesomeRegular.name
    readonly property string solid: fontAwesomeSolid.name

    AwesomeVariables {
        id: variables
    }
}
