import QtQuick 2.4
import Malepy 1.0

Item {
    property string value: ""

    onValueChanged: setValue(value)

    function setValue(val) {
        digit1.value = val.charAt(0)
        digit2.value = val.charAt(1)
        digit3.value = val.charAt(2)
        digit4.value = val.charAt(3)
        digit5.value = val.charAt(4)
        digit6.value = val.charAt(5)
        digit7.value = val.charAt(6)
    }

    Row {
        anchors.fill: parent

        Item {
            height: parent.height; width: parent.width*7/77

            Digit {
                id: digit1
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
                visible: value !== ""
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/77
            sourceComponent: point
        }
        Item {
            height: parent.height; width: parent.width*7/77

            Digit {
                id: digit2
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
                visible: value !== ""
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/77
            sourceComponent: point
        }
        Item {
            height: parent.height; width: parent.width*7/77

            Digit {
                id: digit3
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
                visible: value !== ""
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/77
            sourceComponent: point
        }
        Item {
            height: parent.height; width: parent.width*7/77

            Digit {
                id: digit4
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
                visible: value !== ""
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/77
            sourceComponent: point
        }
        Item {
            height: parent.height; width: parent.width*7/77

            Digit {
                id: digit5
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
                visible: value !== ""
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/77
            sourceComponent: point
        }
        Item {
            height: parent.height; width: parent.width*7/77

            Digit {
                id: digit6
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
                visible: value !== ""
            }
        }
        Loader {
            height: parent.height
            width: parent.width*3/77
            sourceComponent: point
        }
        Item {
            height: parent.height; width: parent.width*7/77

            Digit {
                id: digit7
                height: parent.height; width: parent.width*3/4
                anchors.centerIn: parent
                visible: value !== ""
            }
        }
        Item {
            height: parent.height; width: parent.width*3/77
        }
        MessageUnits {
            height: parent.height; width: parent.width*7/77
        }
    }

    Component {
        id: point

        Item {
            Rectangle {
                width: 10; height: 2*width
                radius: 5
                color: "black"
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.bottom: parent.bottom
            }
        }
    }
}
