import QtQuick 2.4
import Malepy 1.0

Item {
    Column {
        anchors.fill: parent

        Item {
            width: parent.width; height: parent.height/2

            Text {
                anchors.centerIn: parent
                font.pixelSize: parent.height
                font.bold: true
                text: "Hz"
            }
        }
        Row {
            width: parent.width; height: parent.height/2
            spacing: 2

            Item {
                height: parent.height; width: (parent.width-parent.spacing)/2
                anchors.margins: 2

                Text {
                    anchors.centerIn: parent
                    font.pixelSize: parent.height
                    font.bold: true
                    text: "V"
                }
            }
            Item {
                height: parent.height; width: (parent.width-parent.spacing)/2
                anchors.margins: 2

                Text {
                    anchors.centerIn: parent
                    font.pixelSize: parent.height
                    font.bold: true
                    text: "%"
                }
            }
        }
    }
}
