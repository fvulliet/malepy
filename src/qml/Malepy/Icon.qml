import QtQuick 2.4
import Malepy 1.0

Item {
    property bool blinking: false
    property alias lib: myIcon.font.family
    property alias code : myIcon.text
    property alias color: myIcon.color

    width: height

    SequentialAnimation {
        loops: Animation.Infinite
        running: blinking

        PropertyAnimation {
            target: myRect
            property: "opacity"; from: 1; to: 0
        }
        PauseAnimation {
            duration: 250
        }
        PropertyAnimation {
            target: myRect
            property: "opacity"; from: 0; to: 1
        }
        PauseAnimation {
            duration: 250
        }
    }

    Rectangle {
        id: myRect
        height: parent.height/2; width: parent.width/2
        anchors.centerIn: parent
        border.color: "black"
        border.width: 1
        opacity: 1
    }

    Text {
        id: myIcon
        antialiasing: true
        scale: 1
        color: "black"
        font.pixelSize: parent.height
        opacity: 0
    }
}
