pragma Singleton

import QtQuick 2.0
import Malepy 1.0

Item {
    id: fonts

    // public properties
    property string caFont: arialRegular

    property alias arialRegular: arialRegularLoader.name
    property alias fontAwesome: fontAwesomeLoader.name
    property alias faRegular: fontAwesomeRegularLoader.name
    property alias faSolid: fontAwesomeSolidLoader.name

    // Arial
    readonly property FontLoader arialRegularLoader: FontLoader {
        id: arialRegularLoader

        name: "Arial Unicode MS Regular"
        source: "./fonts/ARIALUNI.ttf"
    }

    // FontAwesome
    readonly property FontLoader fontAwesomeLoader: FontLoader {
        id: fontAwesomeLoader

        name: "FontAwesome"
        source: "./fonts/fontawesome-webfont.ttf"
    }

    readonly property FontLoader fontAwesomeRegularLoader: FontLoader {
        id: fontAwesomeRegularLoader

        name: "Font Awesome 5 Free"
        source: "./fonts/fa-regular-400.ttf"
    }

    readonly property FontLoader fontAwesomeSolidLoader: FontLoader {
        id: fontAwesomeSolidLoader

        name: "Font Awesome 5 Free Solid"
        source: "./fonts/fa-solid-900.ttf"
    }
}

