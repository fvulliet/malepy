/******************************************************************************/
// File:        main.cpp
// Description: Entry point of the application
/******************************************************************************/

//int main(int argc, char* argv[])
//{
////    // Creation of the GUI application
////    QCoreApplication app(argc, argv);
//    for(;;)
//    return /*app.exec()*/1;
//}

#include <QQmlContext>
#include <QQmlEngine>
#include <QQuickView>
#include <QStandardPaths>
#include <QFontDatabase>
#include <qqml.h>
#include <QDebug>
#include <QDir>

#include "MalepyQml.hpp"
#include "MyQuickView.hpp"
#include "MyGuiApplication.hpp"

int main(int argc, char *argv[])
{
    MyGuiApplication app(argc, argv);
    app.setApplicationName("Malepy");

    QDir dir(":/");
    for (auto entry : dir.entryList()) {
        qWarning() << "Found entry in resources:" << entry;
    }

    MyQuickView view;
    // load all plugins
    MalepyQml::load(view.engine());

    view.setSource(QUrl("qrc:///qml/Malepy/main.qml"));
    view.setMinimumHeight(600);
    view.setMinimumWidth(800);
    view.setResizeMode(QQuickView::SizeRootObjectToView);
    view.show();

    return app.exec();
}
