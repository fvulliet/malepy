/*
** This document and/or file is SOMFY's property. All information it
** contains is strictly confidential. This document and/or file shall
** not be used, reproduced or passed on in any way, in full or in part
** without SOMFY's prior written approval. All rights reserved.
** Ce document et/ou fichier est la propritye SOMFY. Les informations
** quil contient sont strictement confidentielles. Toute reproduction,
** utilisation, transmission de ce document et/ou fichier, partielle ou
** intégrale, non autorisée préalablement par SOMFY par écrit est
** interdite. Tous droits réservés.
**
** Copyright © (2009-2015), Somfy Activites SA. All rights reserved.
** All reproduction, use or distribution of this software, in whole or
** in part, by any means, without Somfy Activites SA prior written approval, is
** strictly forbidden.
**
** ConnectOSApp.hpp
**
**        Created on: 20/11/2017
**
*/

#ifndef __CONNECTOSAPP_HPP__
#define __CONNECTOSAPP_HPP__

#include <QObject>

#include "core/SdnpClient.hpp"
#include "ConnectOS_export.hpp"

/**
 * @brief The ConnectOSApp class
 */
class CONNECTOS_EXPORT ConnectOSApp: public QObject
{
    Q_OBJECT
    Q_PROPERTY(QString floorPrefix READ floorPrefix WRITE setFloorPrefix
               NOTIFY floorPrefixChanged)
    Q_PROPERTY(QString orientationPrefix READ orientationPrefix
               WRITE setOrientationPrefix NOTIFY orientationPrefixChanged)
    Q_PROPERTY(QString zonePrefix READ zonePrefix WRITE setZonePrefix
               NOTIFY zonePrefixChanged)
    Q_PROPERTY(bool closureWarning READ closureWarning WRITE setClosureWarning
               NOTIFY closureWarningChanged)
    Q_PROPERTY(QString remoteUrl READ remoteUrl WRITE setRemoteUrl
               NOTIFY remoteUrlChanged)
    Q_PROPERTY(bool prefer24TimeMode READ prefer24TimeMode
               WRITE setPrefer24TimeMode NOTIFY prefer24TimeModeChanged)
    Q_PROPERTY(int langId READ langId WRITE setLangId NOTIFY langIdChanged)
    Q_PROPERTY(bool extendedProjectInfo READ extendedProjectInfo
               WRITE setExtendedProjectInfo NOTIFY extendedProjectInfoChanged)
    Q_PROPERTY(bool termsOfUseAccepted READ termsOfUseAccepted
               WRITE setTermsOfUseAccepted NOTIFY termsOfUseAcceptedChanged)
    Q_PROPERTY(QString hashHtml READ hashHtml
               WRITE setHashHtml NOTIFY hashHtmlChanged)
    Q_PROPERTY(QString webRemotePrefix READ webRemotePrefix
               WRITE setWebRemotePrefix NOTIFY webRemotePrefixChanged)
    Q_PROPERTY(QString adminPassword READ adminPassword WRITE setAdminPassword
               NOTIFY adminPasswordChanged)
    Q_PROPERTY(QString currentAccreditation READ currentAccreditation
               WRITE setCurrentAccreditation NOTIFY currentAccreditationChanged)

public:
    ~ConnectOSApp();

    static const QString m_defaultRemoteUrl;

    /**
     * @brief access to singleton
     * @return
     */
    static ConnectOSApp *instance();

    /**
     * @brief singleton destructor
     */
    static void destroy();

    static const char * floorPrefixStr;
    QString floorPrefix() const;
    void setFloorPrefix(QString pref);

    static const char * orientationPrefixStr;
    QString orientationPrefix() const;
    void setOrientationPrefix(QString pref);

    static const char * zonePrefixStr;
    QString zonePrefix() const;
    void setZonePrefix(QString pref);

    bool closureWarning() const;
    void setClosureWarning(bool val);

    QString remoteUrl() const;
    void setRemoteUrl(QString url);

    static const char * m_prefer24SettingName;
    static const bool m_prefer24DefaultValue;
    bool prefer24TimeMode() const;
    void setPrefer24TimeMode(bool val);

    static const char* m_langIdSettingName;
    static const int m_langIdDefaultValue;
    int langId() const;
    void setLangId(int langId);

    static const char* m_extendedPrjInfoSettingName;
    bool extendedProjectInfo() const;
    void setExtendedProjectInfo(bool val);

    static const char* m_termsOfUseAcceptedName;
    bool termsOfUseAccepted() const;
    void setTermsOfUseAccepted(bool val);

    Q_INVOKABLE QString getDefaultRemoteUrl() const;

    Q_INVOKABLE void retranslatePrefixes();

    static const char* m_hashHtml;
    QString hashHtml() const;
    void setHashHtml(QString hashHtml);

    static const char* m_webRemotePrefix;
    QString webRemotePrefix() const;
    void setWebRemotePrefix(QString pref);

    static const char* adminPasswordName;
    QString adminPassword() const;
    void setAdminPassword(QString value);

    static const char* currentAccreditationName;
    QString currentAccreditation() const;
    void setCurrentAccreditation(QString value);

    static SdnpClient& client();
    SdnpClient& getClient();

public slots:
    void closeApp();
    void onApplicationStateChanged(Qt::ApplicationState state);

signals:
    void floorPrefixChanged();
    void orientationPrefixChanged();
    void zonePrefixChanged();
    void closureWarningChanged();
    void remoteUrlChanged();
    void prefer24TimeModeChanged();
    void langIdChanged();
    void extendedProjectInfoChanged();
    void termsOfUseAcceptedChanged();
    void hashHtmlChanged(QString hashHtml);
    void webRemotePrefixChanged();
    void adminPasswordChanged();
    void currentAccreditationChanged();

protected:
    ConnectOSApp();

    SdnpClient m_client;
    bool m_closureWarning;
    bool m_applicationHasStarted;
};

class CONNECTOS_EXPORT ConnectOSAppPrv : public ConnectOSApp
{
public:
    ConnectOSAppPrv();
};

#endif // __CONNECTOSAPP_HPP__


