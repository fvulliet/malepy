#include <QQmlEngine>
#include <QGuiApplication>
#include <QFile>
#include <QDebug>

#include "MalepyQml.hpp"
#include "../config.h"

#define PLUGINDIR LIBDIR "/qml"
#define PLUGINFILE "libmalepyplugin" LIBEXT

static bool initPluginPath(const QStringList &paths, QQmlEngine *engine)
{
    foreach (const QString &path, paths)
    {
        if (QFile::exists(path + ("/" PLUGINFILE)))
        {
            if (!engine->pluginPathList().contains(path))
                engine->addPluginPath(path);
            return true;
        }
    }
    return false;
}

bool MalepyQml::load(QQmlEngine *engine)
{
    QStringList pluginPaths;
    pluginPaths << (TOP_BUILDDIR "/src/qml/Malepy")
                << (qApp->applicationDirPath() + "/../" LIBDIR "/qml/Malepy")
                << (qgetenv("SYSROOT") +
                    ("/" PREFIX "/" LIBDIR "/qml/Malepy"));
    // tmp workaround for qml plugins on android
    pluginPaths << qApp->applicationDirPath();

    if (!engine)
        qWarning("[MalepyQml]: can't initialize module on a null engine");
    else if (!QFile::exists(":/qml/Malepy/qmldir"))
        qWarning("[MalepyQml]: resources not available");
    else if (!initPluginPath(pluginPaths, engine))
        qWarning("[MalepyQml]: failed to find QML plugin");
    else
    {
        if (!engine->importPathList().contains("qrc:/qml"))
            engine->addImportPath("qrc:/qml");
        return true;
    }

    return false;
}

