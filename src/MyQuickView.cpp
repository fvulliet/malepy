#include "MyQuickView.hpp"

MyQuickView::MyQuickView()
{
}

MyQuickView::MyQuickView(QUrl url) :
    QQuickView(url)
{
}

bool MyQuickView::event(QEvent *event)
{
    bool handled;

    switch (event->type())
    {
    case QEvent::Close:
        // catch the event to execute own code before closure
        event->setAccepted(false);
        handled = true;
        break;

    case QEvent::Quit:
        // actually close the application
        handled = true;
        break;

    default:
        handled = QQuickView::event(event);
    }
    return handled;
}

