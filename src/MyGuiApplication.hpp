#ifndef __MYGUIAPPLICATION_HPP__
#define __MYGUIAPPLICATION_HPP__

#include <QApplication>

class MyGuiApplication : public QApplication
{
    Q_OBJECT

public:
    MyGuiApplication(int &argc, char **argv, int flags = ApplicationFlags);
    ~MyGuiApplication();

public slots:
    /**
     * @brief wrap the QGuiApplication::quit slot, debug purpose so far
     */
    void quitApp();
};

#endif /* __MYGUIAPPLICATION_HPP__ */




