/*
** This document and/or file is SOMFY's property. All information it
** contains is strictly confidential. This document and/or file shall
** not be used, reproduced or passed on in any way, in full or in part
** without SOMFY's prior written approval. All rights reserved.
** Ce document et/ou fichier est la propritye SOMFY. Les informations
** quil contient sont strictement confidentielles. Toute reproduction,
** utilisation, transmission de ce document et/ou fichier, partielle ou
** intégrale, non autorisée préalablement par SOMFY par écrit est
** interdite. Tous droits réservés.
**
** Copyright © (2009-2015), Somfy Activites SA. All rights reserved.
** All reproduction, use or distribution of this software, in whole or
** in part, by any means, without Somfy Activites SA prior written approval, is
** strictly forbidden.
**
** ConnectOSApp.cpp
**
**        Created on: 20/11/2017
**
*/

#include <QDebug>
#include <QCryptographicHash>

#include "ConnectOSApp.hpp"

#include "settings/Settings.hpp"
#include "project/Accreditation.hpp"
#include "project/ProjectManager.hpp"
#include "Singleton.hpp"
#include "Singleton.hxx"

const char* ConnectOSApp::m_hashHtml = "hashHtml";
const char* ConnectOSApp::m_termsOfUseAcceptedName = "termsOfUseAccepted";
const char* ConnectOSApp::m_extendedPrjInfoSettingName = "extendedPrjInfo";
const QString ConnectOSApp::m_defaultRemoteUrl = "http://172.30.6.189:4000/api/";
const char* ConnectOSApp::m_prefer24SettingName = "prefer24TM";
const bool ConnectOSApp::m_prefer24DefaultValue = true;
const char* ConnectOSApp::m_langIdSettingName = "langId";
const int ConnectOSApp::m_langIdDefaultValue = 0;
const char* ConnectOSApp::m_webRemotePrefix = "webRemote";
const char* ConnectOSApp::adminPasswordName = "adminPwd";
const char* ConnectOSApp::currentAccreditationName = "currentAccreditation";
const char* ConnectOSApp::floorPrefixStr = "floorPrefix";
const char* ConnectOSApp::orientationPrefixStr = "orientationPrefix";
const char* ConnectOSApp::zonePrefixStr = "zonePrefix";

class PubConnectOSApp: public ConnectOSApp {};

ConnectOSApp::ConnectOSApp() :
    QObject(),
    m_client(this),
    m_closureWarning(false),
    m_applicationHasStarted(false)
{
    Settings::instance()->setValueIfNotSet(m_extendedPrjInfoSettingName, false);
    Settings::instance()->setValueIfNotSet(m_termsOfUseAcceptedName, false);
    Settings::instance()->setValueIfNotSet(floorPrefixStr, tr("Floor"));
    Settings::instance()->setValueIfNotSet(orientationPrefixStr, tr("Orientation"));
    Settings::instance()->setValueIfNotSet(zonePrefixStr, tr("Zone"));
    Settings::instance()->setValueIfNotSet("remoteUrl", m_defaultRemoteUrl);
    Settings::instance()->setValueIfNotSet(m_prefer24SettingName, m_prefer24DefaultValue);
    Settings::instance()->setValueIfNotSet(m_langIdSettingName, m_langIdDefaultValue);
    Settings::instance()->setValueIfNotSet(m_webRemotePrefix, "Web_remote_Room");
    Settings::instance()->setValueIfNotSet(adminPasswordName, QString());
}

ConnectOSApp::~ConnectOSApp()
{
}

ConnectOSApp *ConnectOSApp::instance()
{
    return &Singleton<PubConnectOSApp>::instance();
}

void ConnectOSApp::destroy()
{
   Singleton<PubConnectOSApp>::destroy();
}



SdnpClient& ConnectOSApp::getClient()
{
    return m_client;
}

SdnpClient& ConnectOSApp::client()
{
    return ConnectOSApp::instance()->getClient();
}

QString ConnectOSApp::floorPrefix() const
{
    return Settings::instance()->value(floorPrefixStr).toString();
}

void ConnectOSApp::setFloorPrefix(QString pref)
{
    Settings::instance()->setValue(floorPrefixStr, pref);
    emit floorPrefixChanged();
}

QString ConnectOSApp::orientationPrefix() const
{
    return Settings::instance()->value(orientationPrefixStr).toString();
}

void ConnectOSApp::setOrientationPrefix(QString pref)
{
    Settings::instance()->setValue(orientationPrefixStr, pref);
    emit orientationPrefixChanged();
}

QString ConnectOSApp::zonePrefix() const
{
    return Settings::instance()->value(zonePrefixStr).toString();
}

void ConnectOSApp::setZonePrefix(QString pref)
{
    Settings::instance()->setValue(zonePrefixStr, pref);
    emit zonePrefixChanged();
}

bool ConnectOSApp::closureWarning() const
{
    return m_closureWarning;
}

void ConnectOSApp::setClosureWarning(bool val)
{
    if (val == m_closureWarning)
        return;

    m_closureWarning = val;
    emit closureWarningChanged();
}

QString ConnectOSApp::remoteUrl() const
{
    return Settings::instance()->value("remoteUrl").toString();
}

void ConnectOSApp::setRemoteUrl(QString url)
{
    Settings::instance()->setValue("remoteUrl", url);
    emit remoteUrlChanged();
}

bool ConnectOSApp::prefer24TimeMode() const
{
    return Settings::instance()->value(m_prefer24SettingName).toBool();
}

void ConnectOSApp::setPrefer24TimeMode(bool val)
{
    Settings::instance()->setValue(m_prefer24SettingName, val);
    emit prefer24TimeModeChanged();
}

int ConnectOSApp::langId() const
{
    return Settings::instance()->value(m_langIdSettingName).toInt();
}

void ConnectOSApp::setLangId(int langId)
{
    Settings::instance()->setValue(m_langIdSettingName, langId);
    langIdChanged();
}

bool ConnectOSApp::extendedProjectInfo() const
{
    return Settings::instance()->value(m_extendedPrjInfoSettingName).toBool();
}

void ConnectOSApp::setExtendedProjectInfo(bool val)
{
    Settings::instance()->setValue(m_extendedPrjInfoSettingName, val);
    emit extendedProjectInfoChanged();
}

bool ConnectOSApp::termsOfUseAccepted() const
{
    return Settings::instance()->value(m_termsOfUseAcceptedName).toBool();
}

void ConnectOSApp::setTermsOfUseAccepted(bool val)
{
    Settings::instance()->setValue(m_termsOfUseAcceptedName, val);
    emit termsOfUseAcceptedChanged();
}

QString ConnectOSApp::getDefaultRemoteUrl() const
{
    return m_defaultRemoteUrl;
}

void ConnectOSApp::retranslatePrefixes()
{
    setFloorPrefix(tr("Floor"));
    setOrientationPrefix(tr("Orientation"));
    setZonePrefix(tr("Zone"));
}

QString ConnectOSApp::hashHtml() const
{
    return Settings::instance()->value(m_hashHtml).toString();
}

void ConnectOSApp::setHashHtml(QString hashHtml)
{
    Settings::instance()->setValue(m_hashHtml, hashHtml);
    emit hashHtmlChanged(m_hashHtml);
}

QString ConnectOSApp::webRemotePrefix() const
{
    return Settings::instance()->value(m_webRemotePrefix).toString();
}

void ConnectOSApp::setWebRemotePrefix(QString pref)
{
    Settings::instance()->setValue(m_webRemotePrefix, pref);
    emit webRemotePrefixChanged();
}

QString ConnectOSApp::adminPassword() const
{
    return Settings::instance()->value(adminPasswordName).toString();
}

void ConnectOSApp::setAdminPassword(QString value)
{
    Settings::instance()->setValue(adminPasswordName, value);
    emit adminPasswordChanged();
}

QString ConnectOSApp::currentAccreditation() const
{
    return Settings::instance()->value(currentAccreditationName).toString();
}

void ConnectOSApp::setCurrentAccreditation(QString value)
{
    Settings::instance()->setValue(currentAccreditationName, value);
    emit currentAccreditationChanged();
}

void ConnectOSApp::closeApp()
{
    qWarning() << "[ConnectOSApp::closeAp] Application ended successfully";
    exit(0);
}

void ConnectOSApp::onApplicationStateChanged(Qt::ApplicationState state)
{
    if (state == Qt::ApplicationActive && !m_applicationHasStarted)
    {
        m_applicationHasStarted = true;
        // once the GUI application has started, let's init the ProjectManager
        ProjectManager::instance()->init();
    }
}

ConnectOSAppPrv::ConnectOSAppPrv() :
    ConnectOSApp()
{
}

