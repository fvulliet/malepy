#include "MyGuiApplication.hpp"


MyGuiApplication::MyGuiApplication(int &argc, char **argv, int flags) :
    QApplication(argc, argv, flags)
{
}

MyGuiApplication::~MyGuiApplication()
{
}

void MyGuiApplication::quitApp()
{
    QApplication::quit();
}
