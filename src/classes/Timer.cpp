#include "Timer.hpp"
#include "Element.hpp"

Timer::Timer() :
    m_count(0),
    m_target(-1)
{
}

void Timer::registerElement(Element* elt)
{
    m_registeredElements.push_back(elt);
}

void Timer::start()
{
    m_target = -1;
}

void Timer::start(int duration)
{
    m_target = duration;
}

void Timer::stop()
{
    m_count = 0;
    m_target = -1;
}

void Timer::newTick()
{
    if (++m_count >= m_target)
    {
        for (unsigned int i=0; i<m_registeredElements.size(); ++i)
        {
            m_registeredElements.at(i)->timeout();
        }
    }
}
