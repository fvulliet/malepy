#include "Screen.hpp"
#include "Icon.hpp"
#include "Digit.hpp"

Screen::Screen()
{
}

Digit*Screen::createDigit(int meas, int col)
{
    if (meas == MEAS12)
    {
        if (col == COL1)
        {
            return new Digit(
                new Segment(1, 1),
                new Segment(1, 2),
                new Segment(1, 3),
                new Segment(1, 4),
                new Segment(1, 5),
                new Segment(1, 6),
                new Segment(1, 7)
            );
        }
        else if (col == COL2)
        {
            return new Digit(
                new Segment(1, 8),
                new Segment(1, 9),
                new Segment(1, 10),
                new Segment(1, 11),
                new Segment(1, 12),
                new Segment(1, 13),
                new Segment(1, 14)
            );
        }
        else if (col == COL3)
        {
            return new Digit(
                new Segment(1, 15),
                new Segment(1, 16),
                new Segment(1, 17),
                new Segment(1, 18),
                new Segment(1, 19),
                new Segment(1, 20),
                new Segment(1, 21)
            );
        }
        else if (col == COL4)
        {
            return new Digit(
                new Segment(1, 22),
                new Segment(1, 23),
                new Segment(1, 24),
                new Segment(1, 25),
                new Segment(1, 26),
                new Segment(1, 27),
                new Segment(1, 28)
            );
        }
        return nullptr;
    }
    else if (meas == MEAS13)
    {
        if (col == COL1)
        {
            return new Digit(
                new Segment(1, 29),
                new Segment(1, 30),
                new Segment(1, 31),
                new Segment(1, 32),
                new Segment(1, 33),
                new Segment(1, 34),
                new Segment(1, 35)
            );
        }
        else if (col == COL2)
        {
            return new Digit(
                new Segment(1, 36),
                new Segment(1, 37),
                new Segment(1, 38),
                new Segment(1, 39),
                new Segment(1, 40),
                new Segment(1, 41),
                new Segment(1, 42)
            );
        }
        else if (col == COL3)
        {
            return new Digit(
                new Segment(1, 43),
                new Segment(1, 44),
                new Segment(1, 45),
                new Segment(1, 46),
                new Segment(1, 47),
                new Segment(1, 48),
                new Segment(1, 49)
            );
        }
        else if (col == COL4)
        {
            return new Digit(
                new Segment(1, 50),
                new Segment(1, 51),
                new Segment(1, 52),
                new Segment(1, 53),
                new Segment(1, 54),
                new Segment(1, 55),
                new Segment(1, 56)
            );
        }
        return nullptr;
    }
    else if (meas == MEAS23)
    {
        if (col == COL1)
        {
            return new Digit(
                new Segment(1, 57),
                new Segment(1, 58),
                new Segment(1, 59),
                new Segment(1, 60),
                new Segment(1, 61),
                new Segment(1, 62),
                new Segment(1, 63)
            );
        }
        else if (col == COL2)
        {
            return new Digit(
                new Segment(1, 64),
                new Segment(1, 65),
                new Segment(1, 66),
                new Segment(1, 67),
                new Segment(1, 68),
                new Segment(1, 69),
                new Segment(1, 70)
            );
        }
        else if (col == COL3)
        {
            return new Digit(
                new Segment(1, 71),
                new Segment(1, 72),
                new Segment(1, 73),
                new Segment(1, 74),
                new Segment(1, 75),
                new Segment(1, 76),
                new Segment(1, 77)
            );
        }
        else if (col == COL4)
        {
            return new Digit(
                new Segment(1, 78),
                new Segment(1, 79),
                new Segment(1, 80),
                new Segment(1, 81),
                new Segment(1, 82),
                new Segment(1, 83),
                new Segment(1, 84)
            );
        }
        return nullptr;
    }
    return nullptr;
}

Segment*Screen::createUnit(int meas)
{
    if (meas == MEAS12)
    {
        return new Segment(2, 1);
    }
    else if (meas == MEAS13)
    {
        return new Segment(2, 2);
    }
    else if (meas == MEAS13)
    {
        return new Segment(2, 2);
    }
    return nullptr;
}

Icon*Screen::createWifiIcon()
{
    return new Icon(0, 0);
}

Icon*Screen::createWapIcon()
{
    return new Icon(0, 1);
}

void Screen::registerElement(Element* elt)
{
    m_timer.registerElement(elt);
}

void Screen::startCounting()
{
    m_timer.start();
}

void Screen::stopCounting()
{
    m_timer.stop();
}
