#include <vector>
#include <iostream>

#include "Digit.hpp"
#include "Segment.hpp"

// comment digit connait le tick de screen ?

enum SegmentLocation
{
    TOP = 0,
    TOP_RIGHT,
    BOTTOM_RIGHT,
    BOTTOM,
    BOTTOM_LEFT,
    TOP_LEFT,
    CENTER,
    MAX_SEGEMENTS
};

Digit::Digit(Segment* top, Segment* topRight, Segment* bottomRight,
    Segment* bottom, Segment* bottomLeft, Segment* topLeft, Segment* center) :
    Element()
{
    m_segments.push_back(top);
    m_segments.push_back(topRight);
    m_segments.push_back(bottomRight);
    m_segments.push_back(bottom);
    m_segments.push_back(bottomLeft);
    m_segments.push_back(topLeft);
    m_segments.push_back(center);
}

void Digit::write(int value)
{
    m_currentValue = value;

    if (value == 8)
    {
        m_segments[TOP]->on();
        m_segments[TOP_RIGHT]->on();
        m_segments[BOTTOM_RIGHT]->on();
        m_segments[BOTTOM]->on();
        m_segments[BOTTOM_LEFT]->on();
        m_segments[TOP_LEFT]->on();
        m_segments[CENTER]->on();
    }
    else if (value == 0)
    {
        m_segments[TOP]->on();
        m_segments[TOP_RIGHT]->on();
        m_segments[BOTTOM_RIGHT]->on();
        m_segments[BOTTOM]->on();
        m_segments[BOTTOM_LEFT]->on();
        m_segments[TOP_LEFT]->on();
        m_segments[CENTER]->off();
    }
}

void Digit::clear()
{
    m_segments[TOP]->off();
    m_segments[TOP_RIGHT]->off();
    m_segments[BOTTOM_RIGHT]->off();
    m_segments[BOTTOM]->off();
    m_segments[BOTTOM_LEFT]->off();
    m_segments[TOP_LEFT]->off();
    m_segments[CENTER]->off();
}

void Digit::startBlinking()
{
    m_state = EltState::BLINKING;
}

void Digit::stopBlinking()
{
    m_state = EltState::IDLE;
}

void Digit::timeout()
{
    if (++m_count >= m_period)
    {
        std::cout << "-----" << m_count << ">=" << m_period << ": Icon::timeout elapsed !";
    }
}
