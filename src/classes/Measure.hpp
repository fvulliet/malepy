#ifndef __MEASURE_H__
#define __MEASURE_H__

#define NB_DIGITS 4

class Segment;
class Digit;

class Measure
{
public:
    Measure();
    Measure(Digit* d1, Digit* d2, Digit* d3, Digit* d4, Segment* unit);
    ~Measure();

    void setDigit1(Digit* d);
    void setDigit2(Digit* d);
    void setDigit3(Digit* d);
    void setDigit4(Digit* d);

    void write(int value, bool unit);
    void edit(int digitIndex);

private:
    Digit* m_digits[NB_DIGITS];
    Segment* m_unit;
};

#endif /* __MEASURE_H__ */
