#ifndef __SEGMENT_H__
#define __SEGMENT_H__

#include <stdint.h>

#include "Element.hpp"

#define NO_ID 0

enum class State : uint8_t
{
    OFF,
    ON
};

class Segment
{
public:
    Segment(int cmd, int bit, uint8_t id = NO_ID);

    uint8_t id() const;
    int command() const;
    int bit() const;

    State state() const;
    void on();
    void off();

private:
    int m_command;
    int m_bit;
    uint8_t m_id;

    State m_state;
};

#endif /* __SEGMENT_H__ */
