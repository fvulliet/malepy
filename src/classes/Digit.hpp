#ifndef __DIGIT_H__
#define __DIGIT_H__

#include "Element.hpp"

class Segment;

#define NB_SEGMENTS 7

class Digit : public Element
{
public:
    Digit(Segment* top, Segment* topRight, Segment* bottomRight,
        Segment* bottom, Segment* bottomLeft, Segment* topLeft, Segment* center);
    virtual ~Digit() {}

    void write(int value);
    void clear();

    void startBlinking();
    void stopBlinking();

    virtual void timeout() override;

private:
    int m_currentValue;
};

#endif /* __DIGIT_H__ */
