#ifndef __DISPLAY_H__
#define __DISPLAY_H__

#include <vector>

enum MeasureType
{
    V12,
    V13,
    V23,
    NB_MEASURES
};

#define ALL_SEGMENTS 250

class Measure;
class Icon;

class Display
{
public:
    Display();
    ~Display();

    void setMeasure12(Measure* meas);
    void setMeasure13(Measure* meas);
    void setMeasure23(Measure* meas);
    void setWifiIcon(Icon* icon);
    void setWapIcon(Icon* icon);

    bool getWap() const;
    void showWap(bool state);

    bool getWifi() const;
    void showWifi(bool state);

    void writeMeas12(int value, bool unit);
    void writeMeas13(int value, bool unit);
    void writeMeas23(int value, bool unit);

    void editMeas12(int digitIndex = 0);
    void editMeas13(int digitIndex = 0);
    void editMeas23(int digitIndex = 0);

private:
    Measure* m_measures[NB_MEASURES];
    Icon* m_wapIcon;
    Icon* m_wifiIcon;
};

#endif /* __DISPLAY_H__ */
