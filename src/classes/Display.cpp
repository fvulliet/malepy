#include <iostream>

#include "Display.hpp"
#include "Segment.hpp"
#include "Digit.hpp"
#include "Measure.hpp"
#include "Icon.hpp"

Display::Display() :
    m_wapIcon(nullptr),
    m_wifiIcon(nullptr)
{
    for (int i=0; i<NB_MEASURES; ++i)
    {
        m_measures[i] = nullptr;
    }
}

Display::~Display()
{
}

void Display::setMeasure12(Measure* meas)
{
    m_measures[V12] = meas;
}

void Display::setMeasure13(Measure* meas)
{
    m_measures[V13] = meas;
}

void Display::setMeasure23(Measure* meas)
{
    m_measures[V23] = meas;
}

void Display::setWifiIcon(Icon* icon)
{
    m_wifiIcon = icon;
}

void Display::setWapIcon(Icon* icon)
{
    m_wapIcon = icon;
}

bool Display::getWap() const
{
    return m_wapIcon->state() == State::ON;
}

void Display::showWap(bool state)
{
    m_wapIcon->setState(state ? State::ON : State::OFF);
}

bool Display::getWifi() const
{
    return m_wifiIcon->state() == State::ON;
}

void Display::showWifi(bool state)
{
    m_wifiIcon->setState(state ? State::ON : State::OFF);
}

void Display::writeMeas12(int value, bool unit)
{
    m_measures[V12]->write(value, unit);
}

void Display::writeMeas13(int value, bool unit)
{
    m_measures[V13]->write(value, unit);
}

void Display::writeMeas23(int value, bool unit)
{
    m_measures[V23]->write(value, unit);
}

void Display::editMeas12(int digitIndex)
{
    m_measures[V12]->edit(digitIndex);
}

void Display::editMeas13(int digitIndex)
{
    m_measures[V13]->edit(digitIndex);
}

void Display::editMeas23(int digitIndex)
{
    m_measures[V23]->edit(digitIndex);
}
