#include "Measure.hpp"
#include "Digit.hpp"
#include "Segment.hpp"

Measure::Measure() :
    m_unit(nullptr)
{
    for (int i=0; i<NB_DIGITS; ++i)
    {
        m_digits[i] = nullptr;
    }
}

Measure::Measure(Digit* d1, Digit* d2, Digit* d3, Digit* d4, Segment* unit) :
    m_unit(unit)
{
    m_digits[0] = d1;
    m_digits[1] = d2;
    m_digits[2] = d3;
    m_digits[3] = d4;
}

Measure::~Measure()
{
    for (int i=0; i<NB_DIGITS; ++i)
    {
        delete m_digits[i];
        m_digits[i] = nullptr;
    }
    delete m_unit;
    m_unit = nullptr;
}

void Measure::setDigit1(Digit* d)
{
    m_digits[0] = d;
}

void Measure::setDigit2(Digit* d)
{
    m_digits[1] = d;
}

void Measure::setDigit3(Digit* d)
{
    m_digits[2] = d;
}

void Measure::setDigit4(Digit* d)
{
    m_digits[3] = d;
}

void Measure::write(int value, bool unit)
{
    m_digits[0]->write(value/1000);
    m_digits[1]->write((value/100) & 100);
    m_digits[2]->write((value/100) & 10);
    m_digits[3]->write(value & 1);

    if (unit)
        m_unit->on();
    else
        m_unit->off();
}

void Measure::edit(int digitIndex)
{
    m_digits[digitIndex]->startBlinking();
}
