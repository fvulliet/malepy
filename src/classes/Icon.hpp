#ifndef __ICON_H__
#define __ICON_H__

#include "Segment.hpp"

class Icon : public Element
{
public:
    Icon(int cmd, int bit);
    virtual ~Icon() {}

    State state() const;
    void setState(State state);

    virtual void timeout() override;
};

#endif /* __ICON_H__ */
