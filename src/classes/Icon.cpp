#include <iostream>

#include "Icon.hpp"
#include "Segment.hpp"

Icon::Icon(int cmd, int bit) :
    Element()
{
    m_segments.push_back(new Segment(cmd, bit));
    m_period = 10;
}

State Icon::state() const
{
    return m_segments.at(0)->state();
}

void Icon::setState(State state)
{
    if (state == State::ON)
        m_segments.at(0)->on();
    else
        m_segments.at(0)->off();
}

void Icon::timeout()
{
    if (++m_count >= m_period)
    {
        std::cout << "-----" << m_count << ">=" << m_period << ": Icon::timeout elapsed !";
        if (state() == State::ON)
            setState(State::OFF);
        else
            setState(State::ON);
    }
}
