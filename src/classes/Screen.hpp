#ifndef __SCREEN_H__
#define __SCREEN_H__

#include "Timer.hpp"

#define WIFI_ICON 100

#define MEASURE1_DIGIT1_SEG1 1
#define MEASURE1_DIGIT1_SEG2 2
#define MEASURE1_DIGIT1_SEG3 3
#define MEASURE1_DIGIT1_SEG4 4
#define MEASURE1_DIGIT1_SEG5 5
#define MEASURE1_DIGIT1_SEG6 6
#define MEASURE1_DIGIT1_SEG7 7

#define MEASURE1_DIGIT2_SEG1 8
#define MEASURE1_DIGIT2_SEG2 9
#define MEASURE1_DIGIT2_SEG3 10
#define MEASURE1_DIGIT2_SEG4 11
#define MEASURE1_DIGIT2_SEG5 12
#define MEASURE1_DIGIT2_SEG6 13
#define MEASURE1_DIGIT2_SEG7 14

enum measRow
{
    MEAS12 = 0,
    MEAS13,
    MEAS23
};

enum digitCol
{
    COL1 = 0,
    COL2,
    COL3,
    COL4
};

class Icon;
class Digit;
class Segment;

class Screen
{

public:
    Screen();

    Digit* createDigit(int meas, int col);
    Segment* createUnit(int meas);
    Icon* createWifiIcon();
    Icon* createWapIcon();
    void registerElement(Element* elt);
    void startCounting();
    void stopCounting();

private:
    Icon* m_wifiIcon;
    Icon* m_wapIcon;
    Digit* m_digit1;
    Digit* m_digit2;
    Timer m_timer;
};

#endif /* __SCREEN_H__ */
