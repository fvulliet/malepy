#ifndef __TIMER_H__
#define __TIMER_H__

#include <vector>

class Element;

class Timer
{
public:
    Timer();

    void registerElement(Element* elt);

    void start();
    void start(int duration);
    void stop();

    void newTick();

private:
    int m_count;
    int m_target;

    std::vector<Element*> m_registeredElements;
};

#endif /* __TIMER_H__ */
