#include <iostream>

#include "Segment.hpp"

Segment::Segment(int cmd, int bit, uint8_t id) :
    m_command(cmd),
    m_bit(bit),
    m_id(id),
    m_state(State::OFF)
{
}

uint8_t Segment::id() const
{
    return m_id;
}

int Segment::command() const
{
    return m_command;
}

int Segment::bit() const
{
    return m_bit;
}

State Segment::state() const
{
    return m_state;
}

void Segment::on()
{
    if (m_state == State::OFF)
    {
        m_state = State::ON;
        std::cout<<"\n-----segment cmd:"<<m_command<<" bit:"<<m_bit<<" state: ON";
    }
}

void Segment::off()
{
    if (m_state == State::ON)
    {
        m_state = State::OFF;
        std::cout<<"\n-----segment cmd:"<<m_command<<" bit:"<<m_bit<<" state: OFF";
    }
}
