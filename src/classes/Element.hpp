#ifndef __ELEMENT_H__
#define __ELEMENT_H__

#include <stdint.h>
#include <vector>

enum class EltState : uint8_t
{
    IDLE,
    BLINKING
};

class Segment;

class Element
{
public:
    Element();

    virtual void timeout() = 0;

protected:
    std::vector<Segment*>m_segments;
    int m_count; // current number of ticks
    int m_period; // nb of ticks needed to define a period
    EltState m_state;
};

#endif /* __ELEMENT_H___ */
