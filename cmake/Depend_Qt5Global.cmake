include(Tools)
include(FindPkgConfig)

# handle external cache
if(EXT_CACHE)
    cmake_minimum_required(VERSION 3.1)
    message(STATUS "Use of external cache : ${EXT_CACHE}")
    # Get options
    load_extern_cache( CACHE_DIR ${EXT_CACHE} VAR_LIST
            "QT_QMAKE_EXECUTABLE"
            "CMAKE_PREFIX_PATH"
          )
else(EXT_CACHE)
    if (TARGET Qt5::qmake AND WIN32)
        get_target_property(QT_QMAKE_EXECUTABLE Qt5::qmake LOCATION)
    endif (TARGET Qt5::qmake AND WIN32)
endif(EXT_CACHE)

find_package(Qt5Core REQUIRED)
message(STATUS "Using Qt ${Qt5Core_VERSION}")

set(QTVERSION "${Qt5Core_VERSION}")
if(${QTVERSION} VERSION_LESS 5.11)
    macro(qt_use_modules)
        # Use the compile definitions defined in the Qt 5 Widgets module
        add_definitions(${Qt5Widgets_DEFINITIONS})
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")
        qt5_use_modules(${ARGN})
    endmacro()
else(${QTVERSION} VERSION_LESS 5.11)
    set(CMAKE_CXX_STANDARD 14)
    macro(qt_use_modules TARGET)
        # Use the compile definitions defined in the Qt 5 Widgets module
        add_definitions(${Qt5Widgets_DEFINITIONS})
        set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Qt5Widgets_EXECUTABLE_COMPILE_FLAGS}")
        foreach(DEP ${ARGN})
            message(STATUS "Linking ${TARGET} with ${DEP}")
            include_directories(${Qt5${DEP}_INCLUDE_DIRS})
            target_link_libraries(${TARGET} Qt5::${DEP})
        endforeach()
    endmacro()
endif(${QTVERSION} VERSION_LESS 5.11)

<<<<<<< HEAD
=======

>>>>>>> hotfix
