
function(ADD_FILES TARGET)
    cmake_parse_arguments(ADD_FILES "" "DESTINATION" "GLOB;FILES" ${ARGN})
    if(NOT ADD_FILES_DESTINATION)
        message(SEND_ERROR "No destination provided")
    endif(NOT ADD_FILES_DESTINATION)

    set(ADD_FILES_FILES ${ADD_FILES_UNPARSED_ARGUMENTS} ${ADD_FILES_FILES})
    if(ADD_FILES_GLOB)
        foreach(FILENAME ${ADD_FILES_GLOB})
            file(GLOB ADD_FILES_GLOB
                RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}
                ${FILENAME})
            list(APPEND ADD_FILES_FILES ${ADD_FILES_GLOB})
        endforeach()
    endif(ADD_FILES_GLOB)

    add_info_files(${ADD_FILES_FILES})
    add_custom_target(${TARGET} ALL
                      SOURCES ${ADD_FILES_FILES})

    foreach(FILENAME ${ADD_FILES_FILES})
        set(SRC "${CMAKE_CURRENT_SOURCE_DIR}/${FILENAME}")
        set(DST "${ADD_FILES_DESTINATION}/${FILENAME}")

        add_custom_command(
            TARGET ${TARGET}
            COMMAND ${CMAKE_COMMAND} -E copy ${SRC} ${DST}
            DEPENDS ${SRC}
            COMMENT "Copying file: ${FILENAME}"
        )
    endforeach(FILENAME)
endfunction(ADD_FILES)

function(COPY_FROM_BIN_TO_SRC TARGET)
    cmake_parse_arguments(ADD_FILES "" "FILES" ${ARGN})

    set(ADD_FILES_FILES ${ADD_FILES_UNPARSED_ARGUMENTS} ${ADD_FILES_FILES})

    add_info_files(${ADD_FILES_FILES})

    add_custom_target(${TARGET} ALL
                      SOURCES ${ADD_FILES_FILES})
     set(ADD_SHORT_FILE "")

     foreach(FILENAME ${ADD_FILES_FILES})
          file(GLOB ADD_FILES_FILES
              RELATIVE ${CMAKE_CURRENT_BINARY_DIR}
              ${FILENAME})
          list(APPEND ADD_SHORT_FILE ${ADD_FILES_FILES})
     endforeach()

    foreach(FILENAME ${ADD_SHORT_FILE})
        set(SRC "${CMAKE_CURRENT_BINARY_DIR}/${FILENAME}")
        set(DST "${CMAKE_CURRENT_SOURCE_DIR}/${FILENAME}")

        add_custom_command(
            TARGET ${TARGET}
            COMMAND ${CMAKE_COMMAND} -E copy ${SRC} ${DST}
            DEPENDS ${SRC}
            COMMENT "Copying file: ${FILENAME}"
        )
    endforeach(FILENAME)
endfunction(COPY_FROM_BIN_TO_SRC)


