include(CheckFunctionExists)
check_function_exists(getopt_long HAVE_GETOPT_LONG)

# framework
include(Depend_Qt)

# tools
if (TESTS)
    include(Depend_cppunit)
endif (TESTS)
