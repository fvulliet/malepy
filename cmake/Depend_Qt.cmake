find_package(Qt5Core REQUIRED)
find_package(Qt5Quick REQUIRED)
find_package(Qt5Gui REQUIRED)

set(QT_LIBRARIES 
	${Qt5Core_LIBRARIES}
	${Qt5Quick_LIBRARIES} 
	${Qt5Gui_LIBRARIES} 
)
set(QT_INCLUDES 
	${Qt5Core_INCLUDE_DIRS}
	${Qt5Quick_INCLUDE_DIRS}
   	${Qt5Gui_INCLUDE_DIRS}
)
set(QT_BINARY_DIR "${_qt5Core_install_prefix}/bin")

include_directories(SYSTEM ${QT_INCLUDES})

